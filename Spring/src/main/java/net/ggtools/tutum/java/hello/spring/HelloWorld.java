package net.ggtools.tutum.java.hello.spring;

import java.util.Date;

public class HelloWorld {
    private final String message;

    private final Date date;

    private final String clientIP;

    private final String serverHostname;

    private final long hitCount;

    public HelloWorld(String message, Date date, String clientIP, String serverHostname, long hitCount) {
        this.message = message;
        this.date = date;
        this.clientIP = clientIP;
        this.serverHostname = serverHostname;
        this.hitCount = hitCount;
    }

    public String getMessage() {
        return message;
    }

    public Date getDate() {
        return date;
    }

    public String getClientIP() {
        return clientIP;
    }

    public String getServerHostname() {
        return serverHostname;
    }

    public long getHitCount() {
        return hitCount;
    }
}
