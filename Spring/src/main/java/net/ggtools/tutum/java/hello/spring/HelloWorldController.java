package net.ggtools.tutum.java.hello.spring;

import org.redisson.Redisson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Date;

@RestController
public class HelloWorldController {
    @Autowired
    private Redisson redisson;

    private String hostName;

    @PostConstruct
    public void init() throws UnknownHostException {
        hostName = InetAddress.getLocalHost().getHostName();
    }

    @RequestMapping("/")
    public HelloWorld sayHello(HttpServletRequest request) throws UnknownHostException {
        return new HelloWorld("Hello World",
                new Date(),
                request.getRemoteAddr(),
                hostName,
                redisson.getAtomicLong("HitCount").incrementAndGet());
    }
}
